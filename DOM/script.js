// var button = document.getElementsByTagName("button")[0];
// button.addEventListener("click", function(){
// 	console.log("Clicked!");
// })

var input = document.getElementById("input");
var button = document.getElementById("enter");
var ul = document.querySelector("ul");


function inputLength() {
	return input.value.length;
}

function createListElement(){
	var li = document.createElement("li");
	li.appendChild(document.createTextNode(input.value));
    ul.appendChild(li);
    input.value = "";
}


function addListKeypress(event) {
if ( inputLength() > 0 && event.keyCode === 13 ) {
		createListElement();
	}
}

function addListClick(){
	if ( inputLength() > 0 ) {
		createListElement();
	}
}

input.addEventListener("keypress", addListKeypress);
button.addEventListener("click", addListClick);

